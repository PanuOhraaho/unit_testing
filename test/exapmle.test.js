const expect = require('chai').expect;
const { equal } = require('assert');
const { assert } = require('console');
const mylib = require('../src/mylib.js');

describe('Unit testing mylib.js', () => {

    let myvar = undefined;

    after(() => console.log('After mylib tests'));

    before(() => {
        myvar = 1;
        console.log('Before testing');
    });


    it('Should return 2 when using sum function with a=1, b=1', () => {
        const result = mylib.sum(1,1);
        expect(result).to.equal(2);
    });

    it('Should return 2 when deviding a=4 by b=2', () => {
        const result = mylib.div(4,2);
        expect(result).to.equal(2);
    })

    it('Should throw error when deviding by 0', () => {
        const result = mylib.div(1,0);
        expect(result).to.equal(true);
    })

    it('Should return 9 when multiplying a=3 by b=3', () => {
        const result = mylib.mult(3,3);
        expect(result).to.equal(9);
    });

    it.skip('Assert foo in not bar', () => {
        assert('foo' !== 'bar');
    });

    it.skip('Myvar should exist', () => {
        should.exist(myvar);
    });

    it.skip('Random', () => expect(mylib.random().to.above(0.5)));
});