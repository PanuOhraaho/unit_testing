module.exports = {
    sum: (a, b) => a+b,
    div: (a, b) => {  if(b == 0) {
        try {
            throw new Error('Cannot devide by zero');
        }catch(err) {
            return true;
        }
      } else {return a/b}},
    mult: (a, b) => a*b,
    random: () => Math.random(),
    arrayGen: () => [1,2,3]
}