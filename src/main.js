const express = require('express');
const app = express();
const PORT = 3000;
const mylib = require('./mylib');

app.get('/', (req, res) => {
    res.send('Hello world');
});

app.get('/add', (req, res) => {
    const a = parseInt(req.query.a);
    const b = parseInt(req.query.b);
    res.send(mylib.sum(a,b));
});

app.get('/div', (req, res) => {
    mylib.zeroD(b);
    const a = parseInt(req.query.a);
    const b = parseInt(req.query.b);
    res.send(mylib.div(a, b));
});

app.get('/mult', (req, res) => {
    const a = parseInt(req.query.a);
    const b = parseInt(req.query.b);
    res.send(mylib.mult(a, b));
});

app.listen(port, () => {
    console.log(`Server: http://localhost:${PORT}`);
});

